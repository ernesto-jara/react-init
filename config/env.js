"use strict";

const fs = require("fs");
const path = require("path");
const paths = require("./paths");

delete require.cache[require.resolve("./paths")];

const NODE_ENV = process.env.NODE_ENV;
if (!NODE_ENV) {
    throw new Error("The NODE_ENV environment variable is required but was not specified.");
}

require("dotenv-expand")(
    require("dotenv").config({
        path: ".env",
    })
);
// We support resolving modules according to `NODE_PATH`.
// This lets you use absolute paths in imports inside large monorepos:
// https://github.com/facebook/create-react-app/issues/253.
// It works similar to `NODE_PATH` in Node itself:
// https://nodejs.org/api/modules.html#modules_loading_from_the_global_folders
// Note that unlike in Node, only *relative* paths from `NODE_PATH` are honored.
// Otherwise, we risk importing Node.js core modules into an app instead of webpack shims.
// https://github.com/facebook/create-react-app/issues/1023#issuecomment-265344421
// We also resolve them to make sure all tools using them work consistently.
const appDirectory = fs.realpathSync(process.cwd());
process.env.NODE_PATH = (process.env.NODE_PATH || "")
    .split(path.delimiter)
    .filter((folder) => folder && !path.isAbsolute(folder))
    .map((folder) => path.resolve(appDirectory, folder))
    .join(path.delimiter);

// Grab NODE_ENV and REACT_APP_* environment variables and prepare them to be
// injected into the application via DefinePlugin in webpack configuration.
const REACT_APP = /^AMPS_/i;

function getClientEnvironment(publicUrl) {
    const raw = Object.keys(process.env)
        .filter((key) => REACT_APP.test(key))
        .reduce(
            (env, key) => {
                env[key] = process.env[key];
                return env;
            },
            {
                BABEL_ENV: process.env.BABEL_ENV || "development",
                TSC_COMPILE_ON_ERROR: process.env.TSC_COMPILE_ON_ERROR || "false",
                HTTPS: process.env.HTTPS || "false",
                HOST: process.env.HOST || "0.0.0.0",
                NODE_ENV: process.env.NODE_ENV || "development",
                PUBLIC_URL: publicUrl,
                WDS_SOCKET_HOST: process.env.WDS_SOCKET_HOST,
                WDS_SOCKET_PATH: process.env.WDS_SOCKET_PATH,
                WDS_SOCKET_PORT: process.env.WDS_SOCKET_PORT,
                GENERATE_SOURCEMAP: process.env.GENERATE_SOURCEMAP || "false",
                INLINE_RUNTIME_CHUNK: process.env.INLINE_RUNTIME_CHUNK || "false",
                EXTEND_ESLINT: process.env.EXTEND_ESLINT || "true",
                IMAGE_INLINE_SIZE_LIMIT: process.env.IMAGE_INLINE_SIZE_LIMIT || "10000",
                DANGEROUSLY_DISABLE_HOST_CHECK: process.env.DANGEROUSLY_DISABLE_HOST_CHECK || "true",
            }
        );
    // Stringify all values so we can feed into webpack DefinePlugin
    const stringified = {
        "process.env": Object.keys(raw).reduce((env, key) => {
            env[key] = JSON.stringify(raw[key]);
            return env;
        }, {}),
    };
    console.log("stringified", stringified);

    return { raw, stringified };
}

module.exports = getClientEnvironment;
