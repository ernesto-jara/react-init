import { connect } from "react-redux";
import { Login } from "../components";
import { submitRequest } from "../modules/users/users.actions";

const stateToProps = null;
const dispatchToProps = {
    submitRequest,
};

export default connect(stateToProps, dispatchToProps)(Login);
