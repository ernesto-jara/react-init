import * as React from "react";
import * as Loadable from "react-loadable";

export const Login = Loadable({
    loader: () => import("./login.container"),
    loading() {
        return <div>Loading...</div>;
    },
});
export const Register = Loadable({
    loader: () => import("./register.container"),
    loading() {
        return <div>Loading...</div>;
    },
});
