import { Action } from "redux";

declare module "app-state" {
    interface BaseAction<T, P> extends Action {
        type: T;
        payload: P;
    }

    interface LoginCredentials {
        userCode: string;
        userPassword: string;
        userFingerPrint: string;
    }

    interface UserState {
        name: string;
        surname: string;
        error: any;
    }

    interface AppState {
        user: UserState;
    }
}
