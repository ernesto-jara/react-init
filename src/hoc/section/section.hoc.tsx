import * as React from "react";
import * as cx from "classnames";

export interface SectionProps {
    children: React.ReactNode;
    section: string;
}

export function Section(props: SectionProps): React.ReactElement<SectionProps> {
    const { children, section } = props;
    const clss = `app-section-${section}`;
    return (
        <div
            className={cx({
                [clss]: true,
            })}
        >
            {children}
        </div>
    );
}
