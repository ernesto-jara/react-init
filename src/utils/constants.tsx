export const LOGIN_ROUTE: string = process.env.AMPS_LOGIN_ROUTE || "";
export const REGISTER_ROUTE: string = process.env.AMPS_LOGIN_ROUTE || "";
export const BASE_URI: string = process.env.AMPS_BASE_URI || "";
export const CREATED = 201;
export const OK = 200;
export const FETCH_TIMEOUT = Number(process.env.AMPS_FETCH_TIMEOUT);
