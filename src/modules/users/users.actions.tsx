import { LOGIN_USER_REQUEST, LOGIN_USER_SUCCESS, LOGIN_USER_FAILED } from "./users.types";

import { BaseAction, LoginCredentials } from "app-state";

export function submitRequest(credentials: LoginCredentials): BaseAction<LOGIN_USER_REQUEST, LoginCredentials> {
    return {
        type: LOGIN_USER_REQUEST,
        payload: credentials,
    };
}

export function submitSuccess(token: string): BaseAction<LOGIN_USER_SUCCESS, string> {
    return {
        type: LOGIN_USER_SUCCESS,
        payload: token,
    };
}

export function submitFailed(error: Error): BaseAction<LOGIN_USER_FAILED, Error> {
    return {
        type: LOGIN_USER_FAILED,
        payload: error,
    };
}

export type SubmitRequestAction = typeof submitRequest;
export type SubmitSuccessAction = typeof submitSuccess;
export type SubmitFailedAction = typeof submitFailed;
