import { UserState } from "app-state";
import { LOGIN_USER_REQUEST, CLEAN_USER_STATE, LOGIN_USER_SUCCESS, LOGIN_USER_FAILED } from "./users.types";
import { AnyAction } from "redux";

export const initUserState = Object.freeze<UserState>({
    name: "",
    surname: "",
    error: null,
});

function same(state: UserState): UserState {
    return {
        ...state,
    };
}

function clean(state: UserState): UserState {
    return {
        ...state,
        name: "",
        surname: "",
        error: null,
    };
}

function success(state: UserState, payload: UserState): UserState {
    return {
        ...state,
        ...payload,
        error: null,
    };
}
function failed(state: UserState, payload: Error): UserState {
    return {
        ...state,
        name: "",
        surname: "",
        error: { ...payload },
    };
}

export function userReducer(userState = initUserState, action: AnyAction): UserState {
    const { type, payload } = action;
    switch (type) {
        case LOGIN_USER_REQUEST:
            return same(userState);
        case LOGIN_USER_SUCCESS:
            return success(userState, payload);
        case LOGIN_USER_FAILED:
            return failed(userState, payload);
        case CLEAN_USER_STATE:
            return clean(userState);
        default:
            return same(userState);
    }
}
