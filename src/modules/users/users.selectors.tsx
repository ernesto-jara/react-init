import { createSelector } from "reselect";
import { AppState } from "app-state";

const name = (state: AppState) => state.user.name;

export const nameSelector = createSelector([name], (name: string) => name);
