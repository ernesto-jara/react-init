import { Api, config } from "../../api";
import { AxiosRequestConfig, AxiosResponse } from "axios";
import { BASE_URI, LOGIN_ROUTE } from "../../utils/constants";
import { LoginCredentials, UserState } from "app-state";

export class UserApi extends Api {
    public constructor(config: AxiosRequestConfig) {
        super(config);

        this.loginUser = this.loginUser.bind(this);
    }

    public loginUser(login: LoginCredentials): Promise<UserState> {
        return this.post<UserState>(`${BASE_URI}${LOGIN_ROUTE}`, JSON.stringify({ ...login })).then(
            (res: AxiosResponse<UserState>) => res.data
        );
    }
}

export const userApi = new UserApi(config);
