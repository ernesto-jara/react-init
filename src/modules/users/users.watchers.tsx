import { fork, takeEvery, ForkEffect } from "redux-saga/effects";
import { LOGIN_USER_REQUEST } from "./users.types";
import { loginUserWorker } from "./users.workers";

function* loginUserWatcher(): Generator<ForkEffect<never>, void, unknown> {
    yield takeEvery(LOGIN_USER_REQUEST, loginUserWorker);
}

export default [fork(loginUserWatcher)];
