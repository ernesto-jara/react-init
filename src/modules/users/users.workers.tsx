import { call, put } from "redux-saga/effects";
import { BaseAction, LoginCredentials, UserState } from "app-state";
import { LOGIN_USER_REQUEST } from "./users.types";
import { userApi } from "./users.api";
import { submitSuccess, submitFailed } from "./users.actions";

export function* loginUserWorker(credentials: BaseAction<LOGIN_USER_REQUEST, LoginCredentials>) {
    try {
        const { payload } = credentials;
        const user = yield call<(login: LoginCredentials) => Promise<UserState>>(userApi.loginUser, payload);

        yield put(submitSuccess(user));
    } catch (error) {
        yield put(submitFailed(error));
    }
}
