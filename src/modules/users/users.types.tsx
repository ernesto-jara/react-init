export const LOGIN_USER_REQUEST = "user/loging-request";
export type LOGIN_USER_REQUEST = typeof LOGIN_USER_REQUEST;
export const LOGIN_USER_SUCCESS = "user/loging-success";
export type LOGIN_USER_SUCCESS = typeof LOGIN_USER_SUCCESS;
export const LOGIN_USER_FAILED = "user/loging-failed";
export type LOGIN_USER_FAILED = typeof LOGIN_USER_FAILED;
export const CLEAN_USER_STATE = "user/clean-state";
export type CLEAN_USER_STATE = typeof CLEAN_USER_STATE;

export type UserTypes = LOGIN_USER_REQUEST | LOGIN_USER_SUCCESS | LOGIN_USER_FAILED | CLEAN_USER_STATE;
