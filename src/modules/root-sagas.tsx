import { all } from "redux-saga/effects";
import userWatchers from "./users/users.watchers";

export default function* rootSagas() {
    yield all([...userWatchers]);
}
