import { Provider } from "react-redux";
import { render } from "react-dom";
import { AppContainer } from "react-hot-loader";
import * as React from "react";
import { unregister } from "./serviceWorker";
import { App } from "./components";
import { redux } from "./redux";
import "./styles/index.scss";
import rootSagas from "./modules/root-sagas";

const root = document.getElementById("root");

async function init(D) {
    const theStore = await redux();
    theStore.runSaga(rootSagas);
    render(
        <AppContainer>
            <Provider store={theStore}>
                <D />
            </Provider>
        </AppContainer>,
        root
    );
}

init(App);

if (module.hot) {
    module.hot.accept("./components", () => {
        const { App } = require("./components");
        init(App);
    });
}

unregister();
