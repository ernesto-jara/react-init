export function redux() {
    if (process.env.NODE_ENV === "production") {
        return import("./prod").then((res) => res.configureStore);
    } else {
        return import("./dev").then((res) => res.configureStore);
    }
}
