import { applyMiddleware, createStore } from "redux";
import createSagaMiddleware from "redux-saga";
import { AppState } from "app-state";

import { rootReducer } from "../modules/root-reducer";
import { initUserState } from "../modules/users/users.reducers";

const init: AppState = {
    user: initUserState,
};

export const configureStore = (function configureStore(initialState: AppState) {
    const sagaMiddleware = createSagaMiddleware();
    const middleware = [sagaMiddleware];

    return {
        ...createStore(rootReducer, initialState, applyMiddleware(...middleware)),
        runSaga: sagaMiddleware.run,
    };
})(init);
