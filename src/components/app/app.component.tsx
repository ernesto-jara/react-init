import * as React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import { hot } from "react-hot-loader/root";
import { Section } from "../../hoc";
import { LOGIN_ROUTE, REGISTER_ROUTE } from "../../utils/constants";
import { Login, Register } from "../../containers";

export function Main(): React.ReactElement {
    return (
        <Section section="layout">
            <BrowserRouter>
                <Section section="header">
                    <header>Header</header>
                </Section>
                <Switch>
                    <Route exact path={LOGIN_ROUTE} component={Login} />
                    <Route exact path={REGISTER_ROUTE} component={Register} />
                </Switch>
            </BrowserRouter>
        </Section>
    );
}

export const App = hot(Main);
