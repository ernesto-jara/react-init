import * as React from "react";
import { RouteComponentProps } from "react-router-dom";
import { Section } from "../../hoc";
import { SubmitRequestAction } from "../../modules/users/users.actions";

export interface LoginProps extends RouteComponentProps {
    submitRequest: SubmitRequestAction;
}

export interface LoginState {
    code: string;
    password: string;
}

export class Login extends React.Component<LoginProps, LoginState> {
    public state: LoginState;

    public constructor(props: LoginProps) {
        super(props);

        this.state = {
            code: "",
            password: "",
        };

        this.onCodeChangeHandler = this.onCodeChangeHandler.bind(this);
        this.onPasswordChangeHandler = this.onPasswordChangeHandler.bind(this);
        this.onCleanHandler = this.onCleanHandler.bind(this);
        this.onSubmitHandler = this.onSubmitHandler.bind(this);
    }

    public onCleanHandler(evt: React.FormEvent<HTMLButtonElement>): void {
        evt.preventDefault();

        this.setState({
            code: "",
            password: "",
        });
    }

    public onSubmitHandler(evt: React.FormEvent<HTMLButtonElement>): void {
        evt.preventDefault();

        const { code, password } = this.state;
        const { submitRequest } = this.props;

        submitRequest({ userCode: code, userPassword: password, userFingerPrint: "192.168.0.4" });
    }

    public onCodeChangeHandler(evt: React.ChangeEvent<HTMLInputElement>): void {
        const { value } = evt.currentTarget;

        this.setState({ code: value });
    }

    public onPasswordChangeHandler(evt: React.ChangeEvent<HTMLInputElement>): void {
        const { value } = evt.currentTarget;

        this.setState({ password: value });
    }

    public render(): React.ReactElement<LoginProps> {
        const { code, password } = this.state;
        return (
            <Section section="login">
                <div>
                    <label htmlFor="input-login-code">Code</label>
                    <input id="input-login-code" name="code" type="text" onChange={this.onCodeChangeHandler} value={code} />
                </div>
                <div>
                    <label htmlFor="input-login-password">Password</label>
                    <input
                        id="input-login-password"
                        name="password"
                        type="password"
                        onChange={this.onPasswordChangeHandler}
                        value={password}
                    />
                </div>
                <div>
                    <button type="button" onClick={this.onCleanHandler}>
                        Clean
                    </button>
                    <button type="button" onClick={this.onSubmitHandler}>
                        Submit
                    </button>
                </div>
            </Section>
        );
    }
}
