import * as qs from "qs";
import { FETCH_TIMEOUT, BASE_URI } from "../utils/constants";

export const config = {
    timeout: FETCH_TIMEOUT,
    baseUrl: BASE_URI,
    headers: {
        common: {
            "Content-Type": "application/json",
            Accept: "application/json",
        },
        post: {
            "Content-Type": "application/json",
            Accept: "application/json",
        },
        get: {
            "Content-Type": "application/json",
            Accept: "application/json",
        },
    },
    paramsSerializer: (params: string): string => qs.stringify(params, { indices: false }),
};
