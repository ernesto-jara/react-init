/// <reference types="node" />
/// <reference types="react" />
/// <reference types="react-dom" />

declare namespace NodeJS {
    interface ProcessEnv {
        readonly AMPS_PORT: string;
        readonly AMPS_MODE: string;
        readonly AMPS_LOGIN_ROUTE: string;
        readonly AMPS_BASE_URI: string;
        readonly AMPS_FETCH_TIMEOUT: string;
        readonly BABEL_ENV: string;
        readonly TSC_COMPILE_ON_ERROR: string;
        readonly HTTPS: string;
        readonly HOST: string;
        readonly NODE_ENV: string;
        readonly PUBLIC_URL: string;
        readonly GENERATE_SOURCEMAP: string;
        readonly INLINE_RUNTIME_CHUNK: string;
        readonly EXTEND_ESLINT: string;
        readonly IMAGE_INLINE_SIZE_LIMIT: string;
        readonly DANGEROUSLY_DISABLE_HOST_CHECK: string;
    }
}

declare module "*.bmp" {
    const src: string;
    export default src;
}

declare module "*.gif" {
    const src: string;
    export default src;
}

declare module "*.jpg" {
    const src: string;
    export default src;
}

declare module "*.jpeg" {
    const src: string;
    export default src;
}

declare module "*.png" {
    const src: string;
    export default src;
}

declare module "*.webp" {
    const src: string;
    export default src;
}

declare module "*.svg" {
    import * as React from "react";

    export const ReactComponent: React.FunctionComponent<React.SVGProps<SVGSVGElement> & { title?: string }>;

    const src: string;
    export default src;
}

declare module "*.module.css" {
    const classes: { readonly [key: string]: string };
    export default classes;
}

declare module "*.module.scss" {
    const classes: { readonly [key: string]: string };
    export default classes;
}

declare module "*.module.sass" {
    const classes: { readonly [key: string]: string };
    export default classes;
}
