"use strict";

module.exports = function babel(api) {
    const { NODE_ENV } = process.env;

    const presets = ["@babel/preset-react", "@babel/preset-typescript"];

    const plugins = [
        [
            "@babel/plugin-transform-runtime",
            {
                corejs: {
                    version: 3,
                    proposals: true,
                },
                useESModules: true,
            },
        ],
        "@babel/plugin-syntax-dynamic-import",
        "babel-plugin-transform-minify-booleans",
    ];
    if (NODE_ENV === "production") {
        presets.unshift([
            "@babel/preset-env",
            {
                targets: {
                    node: "current",
                    esmodules: true,
                },
                useBuiltIns: "entry",
                corejs: 3,
                modules: false,
            },
        ]);

        plugins.push("@babel/plugin-transform-react-inline-elements");
        plugins.push("@babel/plugin-transform-react-constant-elements");
        plugins.push([
            "transform-inline-environment-variables",
            {
                include: [
                    "NODE_ENV",
                    "PUBLIC_URL",
                    "WDS_SOCKET_HOST",
                    "WDS_SOCKET_PATH",
                    "WDS_SOCKET_PORT",
                    "AMPS_PORT",
                    "AMPS_MODE",
                    "AMPS_LOGIN_ROUTE",
                    "AMPS_BASE_URI",
                    "AMPS_FETCH_TIMEOUT",
                ],
            },
        ]);
        plugins.push("transform-remove-debugger");
        plugins.push("transform-remove-console");
    } else {
        api.async();
    }
    api.cache(() => NODE_ENV);
    api.env();

    return {
        presets,
        plugins,
    };
};
