"use strict";

module.exports = {
    singleQuote: false,
    trailingComma: "es5",
    semi: true,
    useTabs: false,
    tabWidth: 4,
    printWidth: 130,
    quoteProps: "as-needed",
    endOfLine: "lf",
    bracketSpacing: true,
    parser: "typescript",
};
